package com.haade.pneplanner;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.haade.pneplanner.retrofit.Categories;
import com.haade.pneplanner.retrofit.Category;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.CustomViewHolder> {
    private List<Category> dataList;
    private Context context;
    public EventsAdapter(Context context,List<Category> dataList){
        this.context = context;
        this.dataList = dataList;
    }
    class CustomViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        TextView txtTitle;
        private ImageView coverImage;
        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            txtTitle = mView.findViewById(R.id.event_cat);
            coverImage = mView.findViewById(R.id.image123);
        }
    }
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.event_view, parent, false);
        return new CustomViewHolder(view);
   }
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.txtTitle.setText(dataList.get(position).getName());
        Picasso.with(context).load(dataList.get(position).getImage()).into(holder.coverImage);
        holder.coverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii=new Intent(context,ActivityScreen2.class);
                context.startActivity(ii);

            }
        });
    }
    @Override
    public int getItemCount() {
    return dataList.size();
    }
}