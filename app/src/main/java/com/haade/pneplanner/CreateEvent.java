package com.haade.pneplanner;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CreateEvent extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "Add Event";
    private static final String addEvent_url = "https://acumaxtechnologies.com/pneplanner/api/addEvent";

    String event_name, event_start_date, event_end_date, event_start_time, event_end_time,
            event_category, radioType,location ,event_price, event_description,user_id;

    String rPublic,rPaid;
    String radioT;

    ProgressDialog progressDialog;
    AlertDialog.Builder builder2;
    ImageView backaddevnt;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String [] arraySpinner;
    EditText myevent;
    TextView start_date, end_date,time_start, time_end;
    EditText locationAdd,addPrice,descriptioN;
    RadioButton publicRadio,paidRadio;
    RadioGroup radioGroup;
    Spinner spin_category;
    TextView amount_shows;
    TextView idShows;

    Button Submit_Button;
    final Context c = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        builder2=new AlertDialog.Builder(CreateEvent.this);

        backaddevnt=(ImageView)findViewById(R.id.backG);
        backaddevnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityScreen.class);
                startActivity(intent);
            }
        });

        myevent=(EditText)findViewById(R.id.addevnt);
        locationAdd=(EditText)findViewById(R.id.addlocation);
        descriptioN=(EditText)findViewById(R.id.adddcrptn);
        addPrice=(EditText)findViewById(R.id.edit_amountShow);
        addPrice.setVisibility(View.GONE);

        spin_category=(Spinner)findViewById(R.id.cat_spinner);
        this.arraySpinner=new String[]{"Art Show","Birthday Party","Dance Party","Dinner Party"};

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spin_category.setAdapter(adapter);
        spin_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                event_category= spin_category.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        radioGroup=(RadioGroup)findViewById(R.id.radio_group);
        publicRadio=(RadioButton)findViewById(R.id.radio_one);
        publicRadio.isChecked();
        radioType=publicRadio.getText().toString();
        paidRadio=(RadioButton)findViewById(R.id.radio_two);

        amount_shows=(TextView)findViewById(R.id.price_amount);
        amount_shows.setVisibility(View.GONE);

        time_start=(TextView) findViewById(R.id.addtm);
        time_start.setInputType(InputType.TYPE_NULL);
        time_start.setOnClickListener(this);

        time_end=(TextView)findViewById(R.id.addtmb);
        time_end.setInputType(InputType.TYPE_NULL);
        time_end.setOnClickListener(this);

        start_date=(TextView)findViewById(R.id.adddate);
        start_date.setInputType(InputType.TYPE_NULL);
        start_date.setOnClickListener(this);

        end_date=(TextView)findViewById(R.id.adddateb);
        end_date.setInputType(InputType.TYPE_NULL);
        end_date.setOnClickListener(this);

        idShows=(TextView)findViewById(R.id.showsId);
        //idShows.setText(String.valueOf(MainActivity.Mid));


        Submit_Button = (Button) findViewById(R.id.done);
        Submit_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Control();
            }
        });

    }

    public String onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radio_one:
                if (checked) {

                    radioType = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
                    //publicRadio.getText();
                    amount_shows.setVisibility(View.GONE);
                    addPrice.setVisibility(View.GONE);
                    rPublic="public";
                    event_price="0";
                }
                break;

            case R.id.radio_two:
                if (checked) {
                    radioType = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
                    //paidRadio.getText();

                    amount_shows.setVisibility(View.VISIBLE);
                    addPrice.setVisibility(View.VISIBLE);
                    addPrice.setText("");
                    rPaid="paid";
                    event_price=addPrice.getText().toString();

                }
                break;
        }
        return null;
    }


    public void Control(){
                //PriceControl();

                user_id          =  idShows.getText().toString();
                event_name       =  myevent.getText().toString();
                event_start_date =  start_date.getText().toString();
                event_end_date   =  end_date.getText().toString();
                event_start_time =  time_start.getText().toString();
                event_end_time   =  time_end.getText().toString();
                location         =  locationAdd.getText().toString();
                event_category   =  spin_category.getSelectedItem().toString();
                //radioType       =   rPublic.getText().toString();
                event_price      =  addPrice.getText().toString();
                event_description=  descriptioN.getText().toString();
//                        Toast.makeText(getApplicationContext(), "  user_id: "+user_id+"  event_name:"+event_name+"  event_start_date:"+
//                        event_start_date+"  end_date:"+event_end_date+"  start_time:"+event_start_time+"  end_time:"+event_end_time
//                        +"  category:"+event_category+"  event_type:"+radioType+"  price:"+event_price+"  description:"+event_description, Toast.LENGTH_SHORT).show();
        if (event_name.equals("")||event_start_time.equals("")|| event_end_time
                .equals("")||event_start_date.equals("")||event_end_date.equals("")||location.equals("")
                ||event_description.equals("")){
                    builder2.setTitle("Something missing");
                    builder2.setMessage("fill all the fields...");
                    builder2.create().show();
        }else
        {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, addEvent_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                        try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        if (status.equals("Event Added")) {
                            builder2.setTitle("PnE Planner..");
                            builder2.setMessage("Submitted ");
                            builder2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    idShows.setText("");
                                    myevent.setText("");
                                    start_date.setText("");
                                    end_date.setText("");
                                    time_start.setText("");
                                    time_end.setText("");
                                    locationAdd.setText("");
                                    spin_category.getAdapter();
                                    publicRadio.setText("");
                                    paidRadio.setText("");
                                    addPrice.setText("");
                                    descriptioN.getText();

                                    Intent intent = new Intent(getApplicationContext(), ActivityScreen.class);
                                    startActivity(intent);
                                }
                            });
                            AlertDialog alertDialog = builder2.create();
                            alertDialog.show();

                        } else {
                            Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Response Error", Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("event_name", event_name);
                    params.put("event_start_date", event_start_date);
                    params.put("event_end_date",event_end_date );
                    params.put("event_start_time", event_start_time);
                    params.put("event_end_time", event_end_time);
                    params.put("location", location);
                    params.put("event_category", event_category);
                    params.put("event_type", radioType);
                    params.put("event_price", event_price);
                    params.put("event_description",event_description);
                    return params;
                }
            };
            Pneton.getInstance(CreateEvent.this).addToRequestQueue(stringRequest);
        }
    }

    public void PriceControl(){
                        if(addPrice.getText().toString().equalsIgnoreCase("")) {

                            Toast.makeText(getApplicationContext(),"Please Enter Price",Toast.LENGTH_LONG).show();;
                        }
                        else if(addPrice.getText().toString().equalsIgnoreCase("0"))
                        {
                            Toast.makeText(getApplicationContext(), "Invalid amount", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "You have Enter $"+event_price, Toast.LENGTH_SHORT).show();
                        }

    }
    @Override
    public void onClick(View v) {
        if (v == start_date) {
            // select start Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                //2018-07-27 =18:00:00
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    start_date.setText(year + "-" + (monthOfYear +  1) + "-" + dayOfMonth);

                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }if (v == end_date) {
            // select close Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    end_date.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

        // Get Current Time
        if (v == time_start) {
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            time_start.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
        if (v == time_end) {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            time_end.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute,false);
            timePickerDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
