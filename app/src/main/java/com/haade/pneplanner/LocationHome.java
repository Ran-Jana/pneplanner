package com.haade.pneplanner;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
    public class LocationHome extends AppCompatActivity {
    TextView add_location;
    ImageView backR;
    String user_id= "all";
    private String TAG = LocationHome.class.getName();
    private ProgressDialog progressDialog;
    String locationDetails_url = "https://acumaxtechnologies.com/pneplanner/api/getLocations";
    JSONArray jsonArray;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "LocationHome";
    ArrayList<DataObject> dataList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_home);
        backR=(ImageView)findViewById(R.id.backB);
        backR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityScreen.class);
                startActivity(intent);
            }
        });
        add_location = (TextView)findViewById(R.id.addlctnad);
        add_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(getApplicationContext(),CreateLocation.class);
                startActivity(i);
            }
        });

        dataList=new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.list_location);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomAdapter(dataList);
        mRecyclerView.setAdapter(mAdapter);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, locationDetails_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                     jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0 ; i<= jsonArray.length();i++)
                    {
                        DataObject location_data = new DataObject();
                        JSONObject pro = jsonArray.getJSONObject(i);
                        location_data.setPrice(pro.getString("price"));
                        location_data.setPro_state(pro.getString("pro_state"));
                        dataList.add(location_data);
                        mAdapter.notifyDataSetChanged();
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Response Error",Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id);
                return params;
            }
        };
        Pneton.getInstance(LocationHome.this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((CustomAdapter) mAdapter).setOnItemClickListener(new CustomAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
               try {
                   Log.i(LOG_TAG,"Clicked"+position);
                   Intent bundle = new Intent(getApplicationContext(), LocationDescription.class);

                   //Storing data into bundle
                   JSONObject pro = jsonArray.getJSONObject(position);
                   bundle.putExtra("id",pro.getString("id"));
                   bundle.putExtra("l1_add", pro.getString("l1_add"));
                   bundle.putExtra("l2_add", pro.getString("l2_add"));
                   bundle.putExtra("pro_state", pro.getString("pro_state"));
                   bundle.putExtra("country", pro.getString("country"));
                   bundle.putExtra("zipcode", pro.getString("zipcode"));
                   bundle.putExtra("owner_name", pro.getString("owner_name"));
                   bundle.putExtra("mobile_number", pro.getString("mobile_number"));
                   bundle.putExtra("price", pro.getString("price"));
                   bundle.putExtra("description", pro.getString("description"));

                   startActivity(bundle);
               }
               catch (Exception e)
               {
                   e.printStackTrace();
               }
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
