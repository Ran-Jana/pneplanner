package com.haade.pneplanner;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class MyEvent extends AppCompatActivity {
    ImageView imageView;
    String user_id= "1";
    private String TAG = MyEvent.class.getName();
    private ProgressDialog progressDialog;
    String eventData_url = "https://acumaxtechnologies.com/pneplanner/api/getEvents";
    JSONArray j_Array;
    private RecyclerView m_Event_list;
    private RecyclerView.Adapter events_Adapter;
    private RecyclerView.LayoutManager m_LayoutManagers;
    ArrayList<EventObject> eventLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_event);
        imageView=(ImageView)findViewById(R.id.backP);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),ActivityScreen.class);
                startActivity(i);
            }
        });
        eventLists=new ArrayList<>();
        m_Event_list = (RecyclerView) findViewById(R.id.list_event);
        m_Event_list.setHasFixedSize(true);
        m_LayoutManagers = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        m_Event_list.setLayoutManager(m_LayoutManagers);
      //  events_Adapter = new EventsAdapter(eventLists,getApplicationContext());
        m_Event_list.setAdapter(events_Adapter);
        getEventList();
    }
        public void getEventList(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, eventData_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json_Object = new JSONObject(response);
                    j_Array = json_Object.getJSONArray("data");
                    for (int i = 0 ; i<= j_Array.length();i++)
                    {
                        JSONObject jO = j_Array.getJSONObject(i);
                        String image= jO.getString("event_category");
                        eventLists.add(new EventObject(image));
                       // eventsData.setPro_category(jO.getString("event_category"));
                        //eventLists.add(new EventObject(category,image));
                      //  events_Adapter= new EventsAdapter(eventLists,getApplicationContext());
                        m_Event_list.setAdapter(events_Adapter);
                       // eventLists.add(eventsData);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Response Error",Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id);
                return params;
            }
        };
        Pneton.getInstance(MyEvent.this).addToRequestQueue(stringRequest);
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        ((EventsAdapter) events_Adapter).setOnItemClickListener(new EventsAdapter.EventClickListener(){
//            @Override
//            public void onItemClick(int position, View v) {
//                try {
//                    Log.i(TAG,"Clicked"+position);
//                    Intent bundle = new Intent(getApplicationContext(), EventDescription.class);
//
//                    //Storing data into bundle
//                    JSONObject pro = j_Array.getJSONObject(position);
//                    bundle.putExtra("id",pro.getString("id"));
//                    bundle.putExtra("event_name",pro.getString("event_name"));
//                    bundle.putExtra("event_start_date",    pro.getString("event_start_date"));
//                    bundle.putExtra("event_end_date", pro.getString("event_end_date"));
//                    bundle.putExtra("event_start_time",   pro.getString("event_start_time"));
//                    bundle.putExtra("event_end_time",   pro.getString("event_end_time"));
//                    bundle.putExtra("event_type",pro.getString("event_type"));
//                    bundle.putExtra("event_price",pro.getString("event_price"));
//                    bundle.putExtra("event_category",     pro.getString("event_category"));
//                    bundle.putExtra("event_description",pro.getString("description"));
//                    bundle.putExtra("location",pro.getString("location"));
//                    startActivity(bundle);
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        });
    //}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
