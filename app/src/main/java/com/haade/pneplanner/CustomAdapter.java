package com.haade.pneplanner;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<DataObject> dataList;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView t_State;
        TextView t_Price;

        public DataObjectHolder(View itemView) {
            super(itemView);
            t_State = (TextView) itemView.findViewById(R.id.placeShow);
            t_Price = (TextView) itemView.findViewById(R.id.priceS);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }


    public CustomAdapter(ArrayList<DataObject> myDataset) {
        dataList = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_view, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int position) {

        DataObject location_data = dataList.get(position);
        holder.t_State.setText(location_data.getPro_state());
        holder.t_Price.setText(location_data.getPrice());
//        holder.t_State.setText(mDataset.get(position).getPro_state());
//        holder.t_Price.setText(mDataset.get(position).getPrice());

    }

    public void addItem(DataObject dataObj, int index) {
        dataList.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        dataList.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
