package com.haade.pneplanner.retrofit;
import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class Time {

    @SerializedName("event_from")
    private String mEventFrom;
    @SerializedName("event_to")
    private String mEventTo;

    public String getEventFrom() {
        return mEventFrom;
    }

    public void setEventFrom(String eventFrom) {
        mEventFrom = eventFrom;
    }

    public String getEventTo() {
        return mEventTo;
    }

    public void setEventTo(String eventTo) {
        mEventTo = eventTo;
    }

}
