package com.haade.pneplanner.retrofit;
import com.google.gson.annotations.SerializedName;


public class CreateResponse {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("redirect")
    private Boolean mRedirect;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getRedirect() {
        return mRedirect;
    }

    public void setRedirect(Boolean redirect) {
        mRedirect = redirect;
    }

}
