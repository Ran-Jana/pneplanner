package com.haade.pneplanner.retrofit;
import com.google.gson.annotations.SerializedName;


public class RequestAccount {
    @SerializedName("email")
    private String mEmail;
    @SerializedName("location")
    private String mLocation;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("role")
    private String mRole;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("zip")
    private String mZip;


    public RequestAccount(String s, String s1, String s2, String s3, String s4, String s5, String s6) {
        this.mUsername = s;
        this.mEmail=s1;
        this.mPassword = s2;
        this.mMobile=s3;
        this.mLocation=s4;
        this.mZip=s5;
        this.mRole=s6;
    }
    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getZip() {
        return mZip;
    }

    public void setZip(String zip) {
        mZip = zip;
    }

}
