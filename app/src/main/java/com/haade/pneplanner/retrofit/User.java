package com.haade.pneplanner.retrofit;
import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("password")
   String mPassword;
    @SerializedName("username")
  String mUsername;

    public User(String s, String s1) {
        this.mUsername = s;
        this.mPassword = s1;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
