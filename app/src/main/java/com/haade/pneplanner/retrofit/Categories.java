
package com.haade.pneplanner.retrofit;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Categories {

    @SerializedName("categories")
    private List<Category> mCategories;
    @SerializedName("message")
    private String mMessage;

    public List<Category> getCategories() {
        return mCategories;
    }

    public void setCategories(List<Category> categories) {
        mCategories = categories;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
