package com.haade.pneplanner.retrofit;
import com.google.gson.annotations.SerializedName;
public class Category {
    @SerializedName("image")
    private String mImage;
    @SerializedName("name")
    private String mName;
    public String getImage() {
        return mImage;
    }
    public void setImage(String image) {
        mImage = image;
    }
    public String getName() {
        return mName;
    }
    public void setName(String name) {
        mName = name;
    }
}
