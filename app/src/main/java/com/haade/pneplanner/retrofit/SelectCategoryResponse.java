package com.haade.pneplanner.retrofit;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
@SuppressWarnings("unused")
public class SelectCategoryResponse {
    @SerializedName("events")
    private ArrayList<Event> mEvents;
    @SerializedName("message")
    private String mMessage;

    public ArrayList<Event> getEvents() {
        return mEvents;
    }

    public void setEvents(ArrayList<Event> events) {
        mEvents = events;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
