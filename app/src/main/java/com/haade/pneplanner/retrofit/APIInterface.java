package com.haade.pneplanner.retrofit;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by user on 05-Oct-18.
 */
public interface APIInterface {
    @POST("/api/signin")
    Call<LoginResponse> createUser(@Body User user);

    @POST("/api/signup")
    Call<CreateResponse> createAccount(@Body RequestAccount user);

    @GET("/api/categories")
    Call<Categories> doGetListResources();

    @GET("/api/events/category/5bbc1d3c82052c13ac5aac57")
    Call<SelectCategoryResponse> getResponse();
   // Call<List<Category>> doGetListResources();

//    Call<Category> doGetListResources();
}
