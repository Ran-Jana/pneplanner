package com.haade.pneplanner.retrofit;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

    @SuppressWarnings("unused")
    public class Event implements Parcelable {
    @SerializedName("approved")
    private Boolean mApproved;
    @SerializedName("attending")
    private Long mAttending;
    @SerializedName("averageRating")
    private Long mAverageRating;
    @SerializedName("bookingLimit")
    private Long mBookingLimit;
        @Override
        public String toString() {
            return "Event{" +
                    "mApproved=" + mApproved +
                    ", mAttending=" + mAttending +
                    ", mAverageRating=" + mAverageRating +
                    ", mBookingLimit=" + mBookingLimit +
                    ", mCategory='" + mCategory + '\'' +
                    ", mCreatedAt='" + mCreatedAt + '\'' +
                    ", mDate='" + mDate + '\'' +
                    ", mDescription='" + mDescription + '\'' +
                    ", mEventType='" + mEventType + '\'' +
                    ", mId='" + mId + '\'' +
                    ", mImages=" + mImages +
                    ", mLocation='" + mLocation + '\'' +
                    ", mName='" + mName + '\'' +
                    ", mOrganiser=" + mOrganiser +
                    ", mPrice=" + mPrice +
                    ", mReviews=" + mReviews +
                    ", mStatus=" + mStatus +
                    ", mTime=" + mTime +
                    ", m_V=" + m_V +
                    ", m_id='" + m_id + '\'' +
                    '}';
        }
    @SerializedName("category")
    private String mCategory;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("date")
    private String mDate;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("eventType")
    private String mEventType;
    @SerializedName("id")
    private String mId;
    @SerializedName("images")
    private List<String> mImages;
    @SerializedName("location")
    private String mLocation;
    @SerializedName("name")
    private String mName;
    @SerializedName("organiser")
    private Organiser mOrganiser;
    @SerializedName("price")
    private Long mPrice;
    @SerializedName("reviews")
    private List<Object> mReviews;
    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("time")
    private Time mTime;
    @SerializedName("__v")
    private Long m_V;
    @SerializedName("_id")
    private String m_id;

        protected Event(Parcel in) {
            byte tmpMApproved = in.readByte();
            mApproved = tmpMApproved == 0 ? null : tmpMApproved == 1;
            if (in.readByte() == 0) {
                mAttending = null;
            } else {
                mAttending = in.readLong();
            }
            if (in.readByte() == 0) {
                mAverageRating = null;
            } else {
                mAverageRating = in.readLong();
            }
            if (in.readByte() == 0) {
                mBookingLimit = null;
            } else {
                mBookingLimit = in.readLong();
            }
            mCategory = in.readString();
            mCreatedAt = in.readString();
            mDate = in.readString();
            mDescription = in.readString();
            mEventType = in.readString();
            mId = in.readString();
            mImages = in.createStringArrayList();
            mLocation = in.readString();
            mName = in.readString();
            if (in.readByte() == 0) {
                mPrice = null;
            } else {
                mPrice = in.readLong();
            }
            byte tmpMStatus = in.readByte();
            mStatus = tmpMStatus == 0 ? null : tmpMStatus == 1;
            if (in.readByte() == 0) {
                m_V = null;
            } else {
                m_V = in.readLong();
            }
            m_id = in.readString();
        }

        public static final Creator<Event> CREATOR = new Creator<Event>() {
            @Override
            public Event createFromParcel(Parcel in) {
                return new Event(in);
            }

            @Override
            public Event[] newArray(int size) {
                return new Event[size];
            }
        };

        public Boolean getApproved() {
        return mApproved;
    }

    public void setApproved(Boolean approved) {
        mApproved = approved;
    }

    public Long getAttending() {
        return mAttending;
    }

    public void setAttending(Long attending) {
        mAttending = attending;
    }

    public Long getAverageRating() {
        return mAverageRating;
    }

    public void setAverageRating(Long averageRating) {
        mAverageRating = averageRating;
    }

    public Long getBookingLimit() {
        return mBookingLimit;
    }

    public void setBookingLimit(Long bookingLimit) {
        mBookingLimit = bookingLimit;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEventType() {
        return mEventType;
    }

    public void setEventType(String eventType) {
        mEventType = eventType;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public List<String> getImages() {
        return mImages;
    }

    public void setImages(List<String> images) {
        mImages = images;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Organiser getOrganiser() {
        return mOrganiser;
    }

    public void setOrganiser(Organiser organiser) {
        mOrganiser = organiser;
    }

    public Long getPrice() {
        return mPrice;
    }

    public void setPrice(Long price) {
        mPrice = price;
    }

    public List<Object> getReviews() {
        return mReviews;
    }

    public void setReviews(List<Object> reviews) {
        mReviews = reviews;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    public Time getTime() {
        return mTime;
    }

    public void setTime(Time time) {
        mTime = time;
    }

    public Long get_V() {
        return m_V;
    }

    public void set_V(Long _V) {
        m_V = _V;
    }

    public String get_id() {
        return m_id;
    }

    public void set_id(String _id) {
        m_id = _id;
    }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.mEventType);
            parcel.writeString(this.mName);


        }
    }
