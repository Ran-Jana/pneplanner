package com.haade.pneplanner;

public class DataObject {
    private String pro_state;
    private String price;

    DataObject (String pro_state, String price){
        this.pro_state = pro_state;
        this.price = price;
    }

    public DataObject() {

    }
    public String getPro_state() {
        return pro_state;
    }

    public void setPro_state(String pro_state) {
        this.pro_state = pro_state;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
