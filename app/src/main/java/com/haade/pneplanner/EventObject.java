package com.haade.pneplanner;

public class EventObject {
    private String pro_category;
    String image;
    public EventObject()
    {
     }
    public EventObject(String pro_category, String image) {
        this.pro_category = pro_category;
        this.image = image;
    }
    public EventObject(String pro_category) {
        this.pro_category = pro_category;
        this.image = image;
    }
    public String getPro_category() {
        return pro_category;
    }
    public void setPro_category(String pro_category) {
        this.pro_category = pro_category;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
}
