package com.haade.pneplanner;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

    public class LocationDescription extends AppCompatActivity {
    ImageView back_option;
    Button book_location;
    String locations_url = "https://acumaxtechnologies.com/pneplanner/api/getLocations";
    String  id,l1_add,l2_add,pro_state,country,zipcode,owner_name,mobile_number,price,description,image1;
    private String TAG = LocationDescription.class.getName();
    private ProgressDialog progressDialog;
    TextView _id,_price,_addrss1,_addrss2,_province,_country,_zip,_description,_ownerName,_mobile,_partyType;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_description);
        _id =(TextView)findViewById(R.id.locationId);
        _addrss1=(TextView)findViewById(R.id.address1);
        _addrss2=(TextView)findViewById(R.id.address2);
        _province=(TextView)findViewById(R.id.state_province);
        _country=(TextView)findViewById(R.id.countryy);
        _zip=(TextView)findViewById(R.id.pinCode);
        _description=(TextView)findViewById(R.id.lctndecp);
        _ownerName=(TextView)findViewById(R.id.ownrnameshow);
        _mobile=(TextView)findViewById(R.id.ownrmobshow);
        _price=(TextView)findViewById(R.id.priceviewa);
        // getIntent():Return the intent that started this activity.
        Intent bundle = getIntent();
        //Getting bundle
        //Bundle bundle = intent.getExtras();
        //Getting data from bundle
        id = bundle.getStringExtra("id");
        l1_add = bundle.getStringExtra("l1_add");
        l2_add = bundle.getStringExtra("l2_add");
        pro_state = bundle.getStringExtra("pro_state");
        country = bundle.getStringExtra("country");
        zipcode = bundle.getStringExtra("zipcode");
        owner_name = bundle.getStringExtra("owner_name");
        mobile_number = bundle.getStringExtra("mobile_number");
        price = bundle.getStringExtra("price");
        description = bundle.getStringExtra("description");
        //Binding values to TextViews
        _id.setText(id);
        _addrss1.setText(l1_add);
        _addrss2.setText(l2_add);
        _province.setText(pro_state);
        _country.setText(country);
        _zip.setText(zipcode);
        _description.setText(description);
        _ownerName.setText(owner_name);
        _mobile.setText(mobile_number);
        _price.setText(price);
        //Location Book Button
        book_location=(Button)findViewById(R.id.booklctan);
        book_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.acumaxtechnologies.com/"));
                startActivity(mBrowser);
            }
        });
        //Activity Back Option
        back_option=(ImageView)findViewById(R.id.backC);
        back_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),LocationHome.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
