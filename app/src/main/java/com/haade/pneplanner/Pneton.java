package com.haade.pneplanner;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class Pneton {
    private static Pneton mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;
    private Pneton(Context context) {
        mCtx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized Pneton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Pneton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T>void addToRequestQueue(StringRequest stringRequest)
    {
        requestQueue.add(stringRequest);
       // getRequestQueue().add(stringRequest);
    }

//    public <T> void addToRequestQueue(RequestAccount<T> req, String tag) {
//        req.setTag(tag);
//        getRequestQueue().add(req);
//    }
}
