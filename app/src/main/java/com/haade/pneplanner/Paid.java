package com.haade.pneplanner;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.haade.pneplanner.retrofit.Event;
import java.util.ArrayList;
public class Paid extends Fragment {
    ArrayList<Event> data;
    RecyclerView bdayRecycleView;
    private RecyclerView.Adapter event_Adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public static Paid newInstance(ArrayList<Event> data) {
        Paid fragment = new Paid();
        Bundle args = new Bundle();
        args.putParcelableArrayList("data", data);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vww= inflater.inflate(R.layout.fragment_paid, container, false);
        if (getArguments() != null) {
            data  = getArguments().getParcelableArrayList("data");
            bdayRecycleView = vww.findViewById(R.id.bdayRecycleView);
            event_Adapter = new PublicAdapter(data,getActivity());
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            bdayRecycleView.setLayoutManager(layoutManager);
            bdayRecycleView.setAdapter(event_Adapter);

        }
        return vww;
    }
}
