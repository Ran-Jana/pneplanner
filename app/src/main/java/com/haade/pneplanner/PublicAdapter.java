package com.haade.pneplanner;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.haade.pneplanner.retrofit.Event;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class PublicAdapter extends RecyclerView.Adapter<PublicAdapter.EventObjectHolder> {

    private ArrayList<Event> eventList;
    Context context;
    @NonNull
    @Override
    public EventObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_view, parent, false);
        EventObjectHolder eventObjectHolder = new EventObjectHolder(view);
        return eventObjectHolder;
    }
    public PublicAdapter(ArrayList<Event> data, Context context) {
        this.eventList = data;
        this.context=context;
    }
    public class EventObjectHolder extends RecyclerView.ViewHolder {
        TextView _catagery;
        ImageView image;
        public EventObjectHolder(View itemView) {
            super(itemView);
            _catagery = (TextView) itemView.findViewById(R.id.event_cat);
            image = (ImageView) itemView.findViewById(R.id.image123);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull EventObjectHolder holder, int position) {
        final Event event_data = eventList.get(position);
        holder._catagery.setText(event_data.getName());
        final Context context=holder.itemView.getContext();
        Picasso.with(context).load(event_data.getImages().get(0)).into(holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ii=new Intent(context,EventDescription.class);
                ii.putExtra("image",event_data.getImages().get(0));
                ii.putExtra("desc",event_data.getDescription());
                ii.putExtra("category",event_data.getCategory());
                ii.putExtra("price",event_data.getPrice());
                context.startActivity(ii);
            }
        });
    }
    public void addItem(Event eventObj, int index) {
        eventList.add(index, eventObj);
        notifyItemInserted(index);
    }
    public void deleteItem(int index) {
        eventList.remove(index);
        notifyItemRemoved(index);
    }
    @Override
    public int getItemCount() {
        return eventList.size();
    }
}
