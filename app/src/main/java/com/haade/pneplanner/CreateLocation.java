package com.haade.pneplanner;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class CreateLocation extends AppCompatActivity {
    private static final String TAG = "CreateLocation";
    private static final String addlocation_url = "https://acumaxtechnologies.com/pneplanner/api/addLocations";

    String l1_add,l2_add,pro_state,country,zipcode,owner_name,mobile_number,price,description,image1,image2,image3,image4,user_id;
    ProgressDialog progressDialog;
    AlertDialog.Builder builder4;

    EditText first_line,second_line,cntry,stateA,zipA,ownerName,ownerMob,priceA,location_decrptn;
    TextView idShow;
    Button submit_image,submit_info;
    ImageView back, location_image,location_imageB,location_imageC,location_imageD;

    private int PICK_IMAGE_REQUEST = 4;
    Bitmap bitmapc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_location);

        back=(ImageView)findViewById(R.id.backE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iiiii=new Intent(getApplicationContext(),LocationHome.class);
                startActivity(iiiii);
            }
        });
        builder4=new AlertDialog.Builder(CreateLocation.this);

        first_line=(EditText)findViewById(R.id.addaddress);
        second_line=(EditText)findViewById(R.id.addaaaadrs);
        stateA=(EditText)findViewById(R.id.addstate);
        cntry=(EditText)findViewById(R.id.addcntry);
        zipA=(EditText)findViewById(R.id.addzip);
        ownerName=(EditText)findViewById(R.id.ownername);
        ownerMob=(EditText)findViewById(R.id.edit_decmob);
        priceA=(EditText)findViewById(R.id.addlctnprize);
        location_decrptn=(EditText)findViewById(R.id.addlctndcrptn);
        location_image=(ImageView)findViewById(R.id.iimageView);
        location_imageB=(ImageView)findViewById(R.id.iimageView2);
        location_imageC=(ImageView)findViewById(R.id.iimageView3);
        location_imageD=(ImageView)findViewById(R.id.iimageView4);
        idShow=(TextView)findViewById(R.id.showId);
       // idShow.setText(String.valueOf(MainActivity.Mid));

        submit_image=(Button)findViewById(R.id.addimg);
        submit_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
//                Intent intent = new Intent();
//                // Show only images, no videos or anything else
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                // Always show the chooser (if there are multiple options available)
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        submit_info=(Button)findViewById(R.id.sbmt);
        submit_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog=new ProgressDialog(CreateLocation.this);
                progressDialog.setMessage("Uploading, please wait...");
                progressDialog.show();

                user_id=idShow.getText().toString();
                l1_add=first_line.getText().toString();
                l2_add=second_line.getText().toString();
                pro_state=stateA.getText().toString();
                country=cntry.getText().toString();
                zipcode=zipA.getText().toString();
                owner_name=ownerName.getText().toString();
                mobile_number=ownerMob.getText().toString();
                price=priceA.getText().toString();
                description=location_decrptn.getText().toString();
                //image1=location_image.

                if (l1_add.equals("")||l2_add.equals("")||pro_state.equals("")||country.equals("")||zipcode.equals("")||owner_name
                        .equals("")||mobile_number.equals("")||price.equals("")||description.equals("")){
                    builder4.setTitle("Something went wrong...");
                    builder4.setMessage("Please fill all the fields...");
                    builder4.create().show();

                }else {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, addlocation_url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject=new JSONObject(response);
                                String status=jsonObject.getString("status");
                                if (status.equals("Event Added")){
                                    builder4.setTitle("PnE Planner..");
                                    builder4.setMessage("Event Added");
                                    builder4.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            first_line.setText("");
                                            second_line.setText("");
                                            cntry.setText("");
                                            stateA.setText("");
                                            zipA.setText("");
                                            ownerName.setText("");
                                            ownerMob.setText("");
                                            priceA.setText("");
                                            location_decrptn.setText("");
                                            idShow.setText("");

                                            Intent intent = new Intent(getApplicationContext(), ActivityScreen.class);
                                            startActivity(intent);
                                        }
                                    });
                                    AlertDialog alertDialog = builder4.create();
                                    alertDialog.show();
                                }
                                else {
                                    Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Response Error", Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("l1_add", l1_add);
                            params.put("l2_add", l2_add);
                            params.put("pro_state", pro_state);
                            params.put("country", country);
                            params.put("zipcode", zipcode);
                            params.put("owner_name", owner_name);
                            params.put("mobile_number", mobile_number);
                            params.put("price", price);
                            params.put("description", description);
                            params.put("user_id", user_id);
                            //params.put("image1",image1);
                            return params;
                        }
                    };
                    Pneton.getInstance(CreateLocation.this).addToRequestQueue(stringRequest);
                }
            }
        });
    }

    private void selectImage(){
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CreateLocation.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);
                    location_image.setImageBitmap(bitmap);
                    String path = android.os.Environment.getExternalStorageDirectory()+ File.separator + "Phoenix" + File.separator + "default";

                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    }
                    catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.w("path of image from gallery......******************.........", picturePath+"");
                location_image.setImageBitmap(thumbnail);
            }
        }
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//
//            Uri uri = data.getData();
//
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//                // Log.d(TAG, String.valueOf(bitmap));
//                location_image.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
