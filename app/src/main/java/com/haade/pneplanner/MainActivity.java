package com.haade.pneplanner;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.facebook.FacebookSdk;
import com.haade.pneplanner.retrofit.APIClient;
import com.haade.pneplanner.retrofit.APIInterface;
import com.haade.pneplanner.retrofit.LoginResponse;
import com.haade.pneplanner.retrofit.User;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.READ_CONTACTS;

public class MainActivity extends AppCompatActivity  {
//    implements LoaderManager.LoaderCallbacks<Cursor>


    private static final String URL_SocalLogin = "http://acumaxtechnologies.com/pneplanner/api/sociallogin";
    private static final String URL_FOR_LOGIN = "https://acumaxtechnologies.com/pneplanner/api/login";
    String email,password;
    EditText email_enter,password_enter;
    AlertDialog.Builder builder2;
    ProgressDialog progressDialog;
    Dialog dialog;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 234;

    CallbackManager callbackManager;

    APIInterface apiInterface;
//    LoginButton fbButton;
    Button locationHome,join_us,simple_login;
//    SharedPreManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        apiInterface = APIClient.getClient().create(APIInterface.class);
       // session = new SharedPreManager(getApplicationContext());
        //Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();
        email_enter = (EditText) findViewById(R.id.user_id);
        password_enter = (EditText) findViewById(R.id.user_pw);
        simple_login = (Button) findViewById(R.id.btn);
        simple_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = email_enter.getText().toString();
                password = password_enter.getText().toString();
                User user = new User(email, password);
                APIInterface api = APIClient.getAPIInterfaces();
                Call<LoginResponse> userCall = api.createUser(user);
                userCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                        if (response.body().getMessage().equalsIgnoreCase("Authentication successfull.")) {
                            Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(MainActivity.this, ActivityScreen.class));
                                  //  .putExtra("Name", email));
//                            intent.putExtra("Name", Mname);
//                            intent.putExtra("Email", MEmail);
                            //startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), "please try again", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        call.cancel();
                    }
                });

//                StringRequest stringRequest=new StringRequest(RequestAccount.Method.POST, URL_FOR_LOGIN, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonObject=new JSONObject(response);
//                            String status=jsonObject.getString("status");
//                            JSONArray cast = jsonObject.getJSONArray("data");
//                            for (int i=0; i<cast.length(); i++) {
//                                JSONObject   profile = cast.getJSONObject(i);
//                                Mname  =     profile.getString("full_name");
//                                MEmail =     profile.getString("email");
//                                Maddress1 =  profile.getString("add_l1");
//                                Maddress2 =  profile.getString("add_l2");
//                                Mmobile =    profile.getString("mobileno");
//                                Mid =        profile.getInt("id");
//                                MState =     profile.getString("province_state");
//                                MZipcode =   profile.getString("zipcode");
//                                Mcountry =   profile.getString("country");
//                            }
//                            if (status.equals("Login Success")){
//                                builder2.setTitle("PnE Planner..");
//                                builder2.setMessage("Login Successful");
//                                builder2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        email_enter.setText("");
//                                        password_enter.setText("");
//                                        session.createLoginSession(Mname,MEmail,Mid);
//                                        Intent intent = new Intent(getApplicationContext(), ActivityScreen.class);
//                                        intent.putExtra("Name",Mname);
//                                        intent.putExtra("Email",MEmail);
//                                        startActivity(intent);
//                                    }
//                                });
//                                AlertDialog alertDialog = builder2.create();
//                                alertDialog.show();
//                            }
//                            else {
//                                Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getApplicationContext(),"Response Error",Toast.LENGTH_LONG).show();
//                        error.printStackTrace();
//                    }
//                }){
//                    @Override
//                    protected Map<String, String> getParams() throws AuthFailureError {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("email",email);
//                        params.put("password",password);
//                        return params;
//                    }
//                };
//                Pneton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
//            }
            }
        });
//        if (session.isLoggedIn() == true){
//            session.checkLogin();
//            Intent i= new Intent(MainActivity.this,ActivityScreen.class);
//            startActivity(i);
//            finish();
//        }
        //fb
//        callbackManager = CallbackManager.Factory.create();
//        final String EMAIL = "email";
//        fbButton = (LoginButton) findViewById(R.id.login_button);
//        fbButton.setReadPermissions(Arrays.asList(EMAIL));
//        fbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Toast.makeText(getApplicationContext(),"Login Sussefull with facebook",Toast.LENGTH_SHORT).show();
//            }
//            @Override
//            public void onCancel() {
//                Log.d("MainActivity", "Canceled");
//            }
//            @Override
//            public void onError(FacebookException exception) {
//                Toast.makeText(getApplicationContext(),"Oppssss... Check Internet Connection",Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        callbackManager = CallbackManager.Factory.create();
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
////                        Intent i =new Intent(MainActivity.this,ActivityScreen.class);
////                        startActivity(i);
//                    }
//                    @Override
//                    public void onCancel() {
//                        // App code
//                    }
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                    }
//                });
//        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
//
//// Configure sign-in to request the user's ID, email address, and basic
//// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
//        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
//        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
//        updateUI(account);
//
//        // Set the dimensions of the sign-in button.
//        SignInButton signInButton = findViewById(R.id.sign_in_button);
//        signInButton.setSize(SignInButton.SIZE_STANDARD);
//        signInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId()) {
//                    case R.id.sign_in_button:
//                        signIn();
//                        break;
//                }
//            }
//
//            private void signIn() {
//                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//                startActivityForResult(signInIntent,RC_SIGN_IN);
//            }
//        });
//
        join_us=(Button)findViewById(R.id.joinus);
        join_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2=new Intent(getApplicationContext(),Registeration.class);
                startActivity(i2);
            }
        });
//
////        session = new SessionManager(getApplicationContext());
////        session.checkLogin();
//
////        Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();
//    }
//
//    private void updateUI(GoogleSignInAccount account) {
//
//    }
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
//        if (requestCode == RC_SIGN_IN) {
//            // The Task returned from this call is always completed, no need to attach
//            // a listener.
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        }
//    }
//    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
//        try {
//            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
//
//            // Signed in successfully, show authenticated UI.
////            Intent intent=new Intent(getApplicationContext(),ActivityScreen.class);
////            startActivity(intent);
////            updateUI(account);
//        } catch (ApiException e) {
//            // The ApiException status code indicates the detailed failure reason.
//            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
//        }
//    }
//
//    @Override
//    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        return null;
//    }
//    @Override
//    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//    }
//    @Override
//    public void onLoaderReset(Loader<Cursor> loader) {
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//    }
//
//
//
//
//
//
//
//


    }
}
