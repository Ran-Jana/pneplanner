package com.haade.pneplanner;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
public class EventDescription extends AppCompatActivity {
    private String TAG = "EventDescription";
    ImageView mainimage;
    long price;
    String id,event_name,event_start_date,event_end_date,event_start_time,event_end_time,event_type,
            event_price,event_category,event_description,location;
    TextView eventName,categoryName,descroption_view,evnt_cost,start_date,end_date,venue_a,start_time,end_time,ids,eventType;
    Button book_event,share_event;
    ImageView imageView;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_description);
        Intent ii=getIntent();
        String image=  ii.getStringExtra("image");
        String des= ii.getStringExtra("desc");
        String category=ii.getStringExtra("category");
        price=ii.getLongExtra("price",1);


        eventName=(TextView)findViewById(R.id.signtextlc);
        mainimage=(ImageView)findViewById(R.id.imageDec) ;
        descroption_view=(TextView)findViewById(R.id.eventdec);
        categoryName=(TextView)findViewById(R.id.event_category);
        evnt_cost=(TextView)findViewById(R.id.priceshow);
        start_date=(TextView)findViewById(R.id.eventstart);
        end_date=(TextView)findViewById(R.id.eventend);
        eventType=(TextView)findViewById(R.id.type);
        venue_a=(TextView)findViewById(R.id.eventvenuea);
        start_time=(TextView)findViewById(R.id.eventtimea);
        end_time=(TextView)findViewById(R.id.eventtimeb);
        ids=(TextView)findViewById(R.id.id_);
        imageView=(ImageView)findViewById(R.id.backI);
        descroption_view.setText(des);
        categoryName.setText(category);
        evnt_cost.setText(""+price);
        Picasso.with(getApplicationContext()).load(image).into(mainimage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityScreen.class);
                startActivity(intent);
            }
        });
        book_event=(Button)findViewById(R.id.btnbuk);
        book_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mBrowser = new Intent(Intent.ACTION_VIEW,Uri.parse("http://www.acumaxtechnologies.com/"));
                startActivity(mBrowser);
            }
        });

        share_event=(Button)findViewById(R.id.sharebtn);
        share_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.acumaxtechnologies.com/");
                startActivity(Intent.createChooser(shareIntent, "Share link using"));
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
