package com.haade.pneplanner;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class Cash_Adapter extends RecyclerView.Adapter<Cash_Adapter.CashObjectHolder> {
    private static String LOG_TAG = "Cash_Adapter";
    private ArrayList<CashObject> cashList;
    private static Cash_Adapter.CashClickListener cashClickListener;

    @NonNull
    @Override
    public CashObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cash_view, parent, false);
        CashObjectHolder cashObjectHolder = new CashObjectHolder(view);
        return cashObjectHolder;
    }

    public class CashObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView _event,_date,_parking,_eventPrice,_parkingPrice;

        public CashObjectHolder(View itemView) {
            super(itemView);
            _event = (TextView) itemView.findViewById(R.id.one);
            _date = (TextView) itemView.findViewById(R.id.date);
            _parking = (TextView) itemView.findViewById(R.id.parking);
            _eventPrice = (TextView) itemView.findViewById(R.id.eventPrice);
            _parkingPrice = (TextView) itemView.findViewById(R.id.parkingPrice);

            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            cashClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(Cash_Adapter.CashClickListener cashClickListener) {
        this.cashClickListener = cashClickListener;
    }

    public Cash_Adapter(ArrayList<CashObject> cashDataset) {
        cashList = cashDataset;
    }

    @Override
    public void onBindViewHolder(@NonNull Cash_Adapter.CashObjectHolder holder, int position) {

        CashObject cash_data = cashList.get(position);
        holder._event.setText(cash_data.getEvent_name());
        holder._date.setText(cash_data.getEventDate());
        holder._parking.setText(cash_data.getParking());
        holder._eventPrice.setText(cash_data.getEventPrice());
        holder._parkingPrice.setText(cash_data.getParkingPrice());
    }

    public void addItem(CashObject cashObj, int index) {
        cashList.add(index, cashObj);
        notifyItemInserted(index);
    }
    public void deleteItem(int index) {
        cashList.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return cashList.size();
    }

    public interface CashClickListener {
        public void onItemClick(int position, View v);
    }
}