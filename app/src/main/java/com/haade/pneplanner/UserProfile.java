package com.haade.pneplanner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;

public class UserProfile extends AppCompatActivity {
    ImageView profileBack;
    ImageView plannerPhoto;
    TextView _id,_name,_mob,_emml,_add1,_add2,_state,_country,_zip;
    //session
//    SharedPreManager session;
//    HashMap hashMap;
//    String keyId;
//    String plannerDetails ="";

    //fb
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    FacebookCallback<LoginResult> callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        //        session = new SharedPreManager(getApplicationContext());
//        hashMap = session.getUserDetails();
//        keyId = (String) hashMap.get(session.KEY_ID);

        _id=(TextView) findViewById(R.id.use_id);
        _name=  (TextView) findViewById(R.id.use_name);
        _mob=   (TextView) findViewById(R.id.use_mob);
        _emml=  (TextView) findViewById(R.id.use_email);
        _add1=  (TextView)findViewById(R.id.use_addd1);
        _add2=  (TextView)findViewById(R.id.use_addd2);
        _state= (TextView)findViewById(R.id.use_province);
        _country=(TextView)findViewById(R.id.use_country);
        _zip=   (TextView)findViewById(R.id.use_zip);

////        _id.setText(String.valueOf(MainActivity.Mid));
//        _name.setText(MainActivity.Mname);
//        _mob.setText(MainActivity.Mmobile);
//        _emml.setText(MainActivity.MEmail);
//        _add1.setText(MainActivity.Maddress1);
//        _add2.setText(MainActivity.Maddress2);
//        _state.setText(MainActivity.MState);
//        _country.setText(MainActivity.Mcountry);
//        _zip.setText(MainActivity.MZipcode);

        profileBack=(ImageView)findViewById(R.id.backM);
        profileBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),ActivityScreen.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
