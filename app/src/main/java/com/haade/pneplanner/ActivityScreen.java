package com.haade.pneplanner;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.haade.pneplanner.retrofit.APIClient;
import com.haade.pneplanner.retrofit.APIInterface;
import com.haade.pneplanner.retrofit.Categories;
import com.haade.pneplanner.retrofit.Category;
import com.haade.pneplanner.retrofit.CreateResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
public class ActivityScreen extends AppCompatActivity  {
    //implements NavigationView.OnNavigationItemSelectedListener
    //SearchView editsearch;
    private static String TAG = "ActivityScreen";
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_screen);
    APIInterface api = APIClient.getAPIInterfaces();
    Call<Categories> call = api.doGetListResources();
    call.enqueue(new Callback<Categories>() {
    @Override
    public void onResponse(Call<Categories> call, retrofit2.Response<Categories> response) {
    if(response.isSuccessful())
    {
       Toast.makeText(getApplicationContext(),"response",Toast.LENGTH_LONG).show();
       generateDataList(response.body().getCategories());
    }
 }
    @Override
    public void onFailure(Call<Categories> call, Throwable t) {
    Toast.makeText(ActivityScreen.this, "Something went wrong...Please try later!"+t, Toast.LENGTH_SHORT).show();
      }

    });

    }
        private void generateDataList(List<Category> photoList) {
        recyclerView = findViewById(R.id.eventList);
        adapter = new EventsAdapter(this,photoList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivityScreen.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
//
//        eventList = new ArrayList<>();
//        mEvent_list = (RecyclerView) findViewById(R.id.eventList);
//        mEvent_list.setHasFixedSize(true);
//        m_LayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//
//        mEvent_list.setLayoutManager(m_LayoutManager);
//        event_Adapter = new EventsAdapter(eventList,getApplicationContext());
//        mEvent_list.setAdapter(event_Adapter);
//      //  getTempList();
//        usrImage = (ImageView) findViewById(R.id.usrImage);
//        usrname = (TextView) findViewById(R.id.usrName);
//        usrEmailId = (TextView) findViewById(R.id.usremail_Id);
//
//
//        session = new SharedPreManager(getApplicationContext());
//        session.checkLogin();
//      //  Toast.makeText(getApplicationContext(), "Login : " + session.isLoggedIn(), Toast.LENGTH_SHORT).show();
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        toolbar.setTitle("PnE Planner");
//        toolbar.setTitleTextColor(getColor(R.color.white));
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//      //  drawer.addDrawerListener(toggle);
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//
//        usrname = (TextView) navigationView.getHeaderView(0).findViewById(R.id.usrName);
//        usrEmailId = (TextView) navigationView.getHeaderView(0).findViewById(R.id.usremail_Id);
//        usrname.setText(ProfileName);
//        usrEmailId.setText(ProfileEmail);
//
//        add_event = (TextView) findViewById(R.id.addeventad);
//        add_event.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent ii = new Intent(getApplicationContext(), CreateEvent.class);
//                startActivity(ii);
//            }
//        });
//
//        locationHomee = (ImageView) findViewById(R.id.location);
//        locationHomee.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent iiii = new Intent(getApplicationContext(), LocationHome.class);
//                startActivity(iiii);
//            }
//        });
//
//        //10-8-18
//        hashMap = session.getUserDetails();
//        keyID = (String) hashMap.get(session.KEY_ID);
//      //  Toast.makeText(this, "Planner ID : " + keyID, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//            finish();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_screen, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        return super.onOptionsItemSelected(item);
//    }
//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        Fragment fragment = null;
//        Class selectclass = null;
//
//        if (id == R.id.nav_profile) {
//            startActivity(new Intent(getApplicationContext(), UserProfile.class));
//
//        }
//
//        else if (id == R.id.nav_event) {
//           // startActivity(new Intent(getApplicationContext(), MyEvent.class));
//
//        }
////
//
//        else if (id == R.id.nav_location) {
//            startActivity(new Intent(getApplicationContext(), MyLocation.class));
//
//        } else if (id == R.id.nav_booklocation) {
//
//        } else if (id == R.id.nav_logout) {
//            session.logoutUser();
//
//        } else if (id == R.id.nav_cashflow) {
//            Intent iintent = new Intent(ActivityScreen.this, CashFlow.class);
//            startActivity(iintent);
//
//        } else if (id == R.id.nav_about) {
//            Intent iiintent = new Intent(ActivityScreen.this, Feedback.class);
//            startActivity(iiintent);
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }
//
}
