package com.haade.pneplanner;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.haade.pneplanner.retrofit.APIClient;
import com.haade.pneplanner.retrofit.APIInterface;
import com.haade.pneplanner.retrofit.Categories;
import com.haade.pneplanner.retrofit.Event;
import com.haade.pneplanner.retrofit.SelectCategoryResponse;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;

public class ActivityScreen2 extends AppCompatActivity {
ArrayList<String> paid=new ArrayList<>();
    private Toolbar toolbar;
   ArrayList<Event> resModel;
   TextView pub_btn,paid_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pub_btn=(TextView)findViewById(R.id.pub_btn) ;
        paid_btn=(TextView)findViewById(R.id.paid_btn) ;
        pub_btn.setTextColor(Color.WHITE);
        paid_btn.setTextColor(Color.GRAY);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_black_24dp);
        pub_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pub_btn.setTextColor(Color.WHITE);
                paid_btn.setTextColor(Color.GRAY);
                getSupportFragmentManager().beginTransaction().add(R.id.frame,new Public().newInstance(customList), "Public Events").addToBackStack(null).commit();
            }
        });
        paid_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pub_btn.setTextColor(Color.GRAY);
                paid_btn.setTextColor(Color.WHITE);
                getSupportFragmentManager().beginTransaction().add(R.id.frame,new Paid().newInstance(defList), "Public Events").addToBackStack(null).commit();
            }
        });

        resModel = new ArrayList<>();
        APIInterface api = APIClient.getAPIInterfaces();
        Call<SelectCategoryResponse> call = api.getResponse();
        call.enqueue(new Callback<SelectCategoryResponse>() {
            @Override
            public void onResponse(Call<SelectCategoryResponse> call, retrofit2.Response<SelectCategoryResponse> response) {
                if(response.isSuccessful())
                {
                    resModel= response.body().getEvents();
                    defList1(resModel);
                    getSupportFragmentManager().beginTransaction().add(R.id.frame,new Public().newInstance(customList), "Public Events").addToBackStack(null).commit();
                }
            }
            @Override
            public void onFailure(Call<SelectCategoryResponse> call, Throwable t) {
                Toast.makeText(ActivityScreen2.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
        getSupportFragmentManager().beginTransaction().add(R.id.frame,new Public().newInstance(customList), "Public Events").addToBackStack(null).commit();

    }
    @Override
    public void onBackPressed() {
    super.onBackPressed();
    finish();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
    ArrayList<Event> defList= new ArrayList<>();
    ArrayList<Event> customList= new ArrayList<>();
    void defList1(ArrayList<Event> data){

        for (int i=0;i<data.size();i++){

            if (data.get(i).getEventType().equalsIgnoreCase("Paid")){

                defList.add(data.get(i));

            } else if (data.get(i).getEventType().equalsIgnoreCase("Public")){
                customList.add(data.get(i));
            }
        }
    }
}

