package com.haade.pneplanner;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.haade.pneplanner.retrofit.Event;
import java.util.ArrayList;

public class Public extends Fragment {
    ArrayList<Event> data;
    RecyclerView bdayRecycleView;
    private RecyclerView.Adapter event_Adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public static Public newInstance(ArrayList<Event> data) {
        Public fragment = new Public();
        Bundle args = new Bundle();
        args.putParcelableArrayList("data", data);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vw =inflater.inflate(R.layout.fragment_public, container, false);
        if (getArguments() != null) {
            data  = getArguments().getParcelableArrayList("data");
            bdayRecycleView = vw.findViewById(R.id.bdayRecycleView);
            event_Adapter = new PublicAdapter(data,getActivity());
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            bdayRecycleView.setLayoutManager(layoutManager);
            bdayRecycleView.setAdapter(event_Adapter);

        }
        return vw;
    }
}
