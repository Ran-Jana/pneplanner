package com.haade.pneplanner;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.haade.pneplanner.retrofit.APIClient;
import com.haade.pneplanner.retrofit.APIInterface;
import com.haade.pneplanner.retrofit.CreateResponse;
import com.haade.pneplanner.retrofit.LoginResponse;
import com.haade.pneplanner.retrofit.RequestAccount;
import com.haade.pneplanner.retrofit.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.android.volley.Request.Method.*;

public class Registeration extends AppCompatActivity {
    private static final String TAG = "RegisterActivity";
    private static final String reg_url = "https://acumaxtechnologies.com/pneplanner/api/register";
    String full_name,mobileno,add_l1,add_l2,province_state,country,zipcode,email,password;
    ProgressDialog progressDialog;
    AlertDialog.Builder builder1;

    private ImageView backoption;
    private Button submit;
    private EditText nameE,mobileE,address1,address2,province_stateE,countryE,zipE,emailE,passwordE;
    private TextView login_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        backoption=(ImageView)findViewById(R.id.backA);
        backoption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iiii=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(iiii);
            }
        });

        builder1=new AlertDialog.Builder(Registeration.this);

        nameE=(EditText)findViewById(R.id.edit_name);
        mobileE=(EditText)findViewById(R.id.edit_mob);
        address1=(EditText)findViewById(R.id.edit_addressone);
        address2=(EditText)findViewById(R.id.edit_addresstwo);
        province_stateE=(EditText)findViewById(R.id.edit_state);
        countryE=(EditText)findViewById(R.id.edit_country);
        zipE=(EditText)findViewById(R.id.edit_zip);
        emailE=(EditText)findViewById(R.id.edit_email);
        passwordE=(EditText)findViewById(R.id.edit_password);

        login_link=(TextView)findViewById(R.id.link_to_login);
        login_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iii= new Intent(getApplicationContext(),MainActivity.class);
                startActivity(iii);
            }
        });

        submit=(Button)findViewById(R.id.submit_btn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                full_name=nameE.getText().toString();
                mobileno=mobileE.getText().toString();
                add_l1=address1.getText().toString();
                add_l2=address2.getText().toString();
                province_state=province_stateE.getText().toString();
                country=countryE.getText().toString();
                zipcode=zipE.getText().toString();
                email=emailE.getText().toString();
                password=passwordE.getText().toString();
                RequestAccount user = new RequestAccount(full_name,email,password,mobileno,country,zipcode,add_l1);
                APIInterface api = APIClient.getAPIInterfaces();
                Call<CreateResponse> userCall = api.createAccount(user);
                userCall.enqueue(new Callback<CreateResponse>() {
                    @Override
                    public void onResponse(Call<CreateResponse> call, retrofit2.Response<CreateResponse> response) {
                        if (response.body().getMessage().equalsIgnoreCase("Your Account created.Please checkout your mail account for activation link.")) {
                            Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Registeration.this, ActivityScreen.class));
                            //  .putExtra("Name", email));
//                            intent.putExtra("Name", Mname);
//                            intent.putExtra("Email", MEmail);
                            //startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "please try again", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<CreateResponse> call, Throwable t) {
                        call.cancel();
                    }
                });


//                if (full_name.equals("")||mobileno.equals("")||add_l1.equals("")||add_l2.equals("")||province_state.equals("")||country
//                        .equals("")||zipcode.equals("")||email.equals("")||password.equals("")){
//                    builder1.setTitle("Something went wrong...");
//                    builder1.setMessage("Please fill all the fields...");
//                    builder1.create().show();
//                    // displayAlert("input_error");
//                }else {
//                    StringRequest stringRequest = new StringRequest(Method.POST, reg_url, new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject jsonObject=new JSONObject(response);
//                                String status=jsonObject.getString("status");
//                                if (status.equals("Registerd Sucessfull")){
//                                    builder1.setTitle("PnE Planner..");
//                                    builder1.setMessage("Registration Successful");
//                                    builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            nameE.setText("");
//                                            mobileE.setText("");
//                                            address1.setText("");
//                                            address2.setText("");
//                                            province_stateE.setText("");
//                                            countryE.setText("");
//                                            zipE.setText("");
//                                            emailE.setText("");
//                                            passwordE.setText("");
//
//                                            Intent intent = new Intent(getApplicationContext(), ActivityScreen.class);
//                                            startActivity(intent);
//                                        }
//                                    });
//                                    AlertDialog alertDialog = builder1.create();
//                                    alertDialog.show();
//                                }
//                                else {
//                                    Toast.makeText(getApplicationContext(),status,Toast.LENGTH_LONG).show();
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }, new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(getApplicationContext(), "Response Error", Toast.LENGTH_LONG).show();
//                            error.printStackTrace();
//                        }
//                    }) {
//                        @Override
//                        protected Map<String, String> getParams() throws AuthFailureError {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("full_name", full_name);
//                            params.put("mobileno", mobileno);
//                            params.put("add_l1", add_l1);
//                            params.put("add_l2", add_l2);
//                            params.put("province_state", province_state);
//                            params.put("country", country);
//                            params.put("zipcode", zipcode);
//                            params.put("email", email);
//                            params.put("password", password);
//                            return params;
//                        }
//                    };
//                    Pneton.getInstance(Registeration.this).addToRequestQueue(stringRequest);
                }

//               full_name= nameE.getText().toString();
//               mobileno=mobileE.getText().toString();
//               add_l1= address1.getText().toString();
//               add_l2= address2.getText().toString();
//                province_state= province_stateE.getText().toString();
//                country= countryE.getText().toString();
//                zipcode= zipE.getText().toString();
//                email= emailE.getText().toString();
//                password= passwordE.getText().toString();
//
//                if (full_name.equals("")||mobileno.equals("")||add_l1.equals("")||add_l2.equals("")||province_state.equals("")||country
//                        .equals("")||zipcode.equals("")||email.equals("")||password.equals("")){
//                    builder1.setTitle("Something went wrong...");
//                    builder1.setMessage("Please fill all the fields...");
//                    displayAlert("input_error");
//                }
//                else
//                {
//                    StringRequest stringRequest=new StringRequest(RequestAccount.Method.POST, reg_url, new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONArray jsonArray=new JSONArray(response);
//                                JSONObject jsonObject=jsonArray.getJSONObject(0);
//                                String code=jsonObject.getString("code");
//                                String message=jsonObject.getString("message");
//                                builder1.setTitle("Server Response...");
//                                builder1.setMessage(message);
//                                displayAlert(code);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }, new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//
//                        }
//                    }){
//                        @Override
//                        protected Map<String, String> getParams() throws AuthFailureError {
//                            Map<String,String> params=new HashMap<String, String>();
//                            params.put("full_name",full_name);
//                            params.put("mobile",mobileno);
//                            params.put("address Line1",add_l1);
//                            params.put("address Line2",add_l2);
//                            params.put("province",province_state);
//                            params.put("country",country);
//                            params.put("zip",zipcode);
//                            params.put("email",email);
//                            params.put("password",password);
//
//                            return params;
//                        }
//                    };
//                    Pneton.getInstance(Registeration.this).addToRequestQueue(stringRequest);
//                }
//
//
//
//                //submitForm();
//
////                AlertDialog.Builder builder = new AlertDialog.Builder(Registeration.this);
////                builder.setMessage("Thanks for registration");
////                builder.setTitle("PnE Planner");
////                builder.setCancelable(true);
////                //builder.setIcon(R.drawable.logoe);
////                builder.setPositiveButton("NEXT", new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        Intent intent = new Intent(Registeration.this,ActivityScreen.class);
////                        startActivity(intent);
////                    }
////                });
////
////                builder.setNegativeButton("THANKS", new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        Toast.makeText(Registeration.this, "Best of luck...", Toast.LENGTH_SHORT).show();
////                    }
////                });
////                builder.create().show();


        });
    }

//    public void displayAlert(final String codes){
//        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                if(codes.equals("req_failed")){
//                    nameE.setText("");
//                    mobileE.setText("");
//                    address1.setText("");
//                    address2.setText("");
//                    province_stateE.setText("");
//                    countryE.setText("");
//                    zipE.setText("");
//                    emailE.setText("");
//                    passwordE.setText("");
//                }
////                if (codes.equals("input_error")){
////                    passwordE.setText("");
////                }
////                else if(codes.equals("req_success")){
////                    finish();
////                }else if(codes.equals("req_failed")){
////                    nameE.setText("");
////                    mobileE.setText("");
////                    address1.setText("");
////                    address2.setText("");
////                    province_stateE.setText("");
////                    countryE.setText("");
////                    zipE.setText("");
////                    emailE.setText("");
////                    passwordE.setText("");
////                }
//            }
//        });
//
//        AlertDialog alertDialog=builder1.create();
//        alertDialog.show();
//    }

//    private void registerUser(final String full_name,
//                              final String mobileno,
//                              final String add_l1,
//                              final String add_l2,
//                              final String province_state,
//                              final String country,
//                              final String zipcode,
//                              final String email,
//                              final String password)
//    {
//        // Tag used to cancel the request
//        String cancel_req_tag = "registeration";
//
//        progressDialog.setMessage("Adding you ...");
//        showDialog();
//
//        StringRequest strReq = new StringRequest(RequestAccount.Method.POST,
//                reg_url, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, "Register Response: " + response.toString());
//                hideDialog();
//
//                try {
//                    JSONObject jObj = new JSONObject(response);
//                    boolean error = jObj.getBoolean("error");
//
//                    if (!error) {
//                        String user = jObj.getJSONObject("user").getString("name");
//                        Toast.makeText(getApplicationContext(),
//                                "Hello " + user +", You are successfully Added!", Toast.LENGTH_SHORT).show();
//
//                        // Launch login activity
//                        Intent intent = new Intent(Registeration.this,MainActivity.class);
//                        startActivity(intent);
//                        finish();
//                    } else {
//
//                        String errorMsg = jObj.getString("error_msg");
//                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Registration Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
//                hideDialog();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting params to register url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("full_name", full_name);
//                params.put("mobileno", mobileno);
//                params.put("add_l1", add_l1);
//                params.put("add_l2", add_l2);
//                params.put("province_state", province_state);
//                params.put("country", country);
//                params.put("zipcode", zipcode);
//                params.put("email", email);
//                params.put("password", password);
//                return params;
//            }
//        };
//        // Adding request to request queue
//        Pneton.getInstance(getApplicationContext()).addToRequestQueue(strReq,cancel_req_tag);
//
//    }
//    private void showDialog() {
//        if (!progressDialog.isShowing())
//            progressDialog.show();
//    }
//
//    private void hideDialog() {
//        if (progressDialog.isShowing())
//            progressDialog.dismiss();
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
